#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from argparse import RawTextHelpFormatter

import logging
import csv
import psycopg2
import psycopg2.extras
from psycopg2.extensions import quote_ident
import os
import sys
import time

SCRIPTNAME = os.path.basename(sys.argv[0])


def parse_arguments(args):
    parser = argparse.ArgumentParser(description="""\
{0} can copy a CSV input file into a PostgreSQL table.

By default it will use the COPY statement, however you can also use
the multi-values insert statement of psycopg2 by specifying --multi=<page_size>

COPY is by far the fastes way of loading data, however if duplicate key violations
could occur it cannot be used.
Inserting bulk data (like a CSV file) row-by-row would be very expensive, therefore
we use the execute_values, which allows multiple rows to be inserted.

As an example, here are some runs using COPY, using row-by-row and using 1000 rows at a time:

{0} demo.csv               -> duration: 4.39130997658
{0} demo.csv --multi=1     -> duration: 57.6395680904
{0} demo.csv --multi=1000  -> duration: 11.3085451126

""".format(SCRIPTNAME), epilog="""\
Examples

  # Generate some demo data
  {0} --generate=1000000 > demo.csv

  # Create the demo table, with a unique index
  {0} --create=unique

  # Recreate the demo table, with a regular index
  {0} --drop --create

  # Insert demo.csv into the demo table using COPY
  {0} demo.csv

  # Insert demo.csv into the demo table using statements with 100 rows each
  {0} --multi=100 demo.csv

  # Insert yourtable.csv into yourtable using statements with 1000 rows each
  {0} --multi=1000 --table=yourtable yourtable.csv
""".format(SCRIPTNAME),
        add_help=False,
        formatter_class=RawTextHelpFormatter)
    parser.add_argument('-v', '--verbose', action='count', help='Increase verbosity', default=0)
    parser.add_argument('--loglevel', help='Explicitly provide loglevel', default='info')
    parser.add_argument('-d', '--database', help='The database to connect to PostgreSQL with', dest='dbname')
    parser.add_argument('-U', '--username', help='database user name', dest='user')
    parser.add_argument('-h', '--host', help='database server host or socket directory')
    parser.add_argument('-p', '--port', help='database server port')
    parser.add_argument('--help', action='store_true')
    parser.add_argument('-g', '--generate', help='Generate N CSV example records (to stdout)', type=int)
    parser.add_argument('--create', dest='create', const='regular', default='', action='store', nargs='?', type=str,
                        help='Create the table demo. Use --create=unique to add a unique index instead of a regular index')
    parser.add_argument('--drop', action='store_true', help='Drop the table demo first')
    parser.add_argument('-m', '--multi', dest='multi', const=1000, default=0, action='store', nargs='?', type=int,
                        help='Use a multi-valued insert statement (instead of COPY), use --multi=PAGESIZE to set the pagesize (default: 1000)')
    parser.add_argument('-s', '--skip-duplicates', default=True,
                        help='When inserting using multi-valued statements, add the ON CONFLICT DO NOTHING clause')
    parser.add_argument('-t', '--table', help='The table to import into/create', default='demo')
    parser.add_argument('file', help='The CSV file to load', nargs='?', type=argparse.FileType('r'), default=sys.stdin)

    parsed = parser.parse_args(args)
    if parsed.help:
        parser.print_help()
        sys.exit(0)

    return parsed


def connect(conn_args):
    conn_args = conn_args.copy()
    conn_args.setdefault('fallback_application_name', SCRIPTNAME)
    conn_args.setdefault('connect_timeout', 3)

    # psycopg2 2.0 compatiblity: It does not understand all libpq keywords, therefore
    # we generate a dsn connection string instead
    dsn = ' '.join(["{0}='{1}'".format(k, v)
                    for k, v in conn_args.items() if v is not None])
    logging.debug('Connecting to {0}'.format(conn_args))
    conn = psycopg2.connect(dsn)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    return conn, cursor


def prepare_table(cursor, table, unique):
    # To avoid any SQL injection, we ensure we quote the table name correctly.
    # psycopg2 has the quote_ident function, which behaves the same as the quote_ident function
    # that you can use inside a PostgreSQL database
    quoted_table = quote_ident(table, cursor)
    quoted_index = quote_ident(table+"_measured_key_idx", cursor)

    logging.info("Creating table {0}".format(table))
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS {0} (
            measured timestamptz not null,
            key text not null,
            value double precision
        )""".format(quoted_table))

    index_sql = "CREATE {0} INDEX IF NOT EXISTS {1} ON {2} (measured, key)".format('UNIQUE' if unique else '', quoted_index, quoted_table)
    logging.info("Creating index {0}".format(index_sql))
    cursor.execute(index_sql)


def load_multi(cursor, file, table, page_size, skip_duplicates):
    statement = """
        INSERT INTO {0}
        VALUES %s""".format(quote_ident(table, cursor))

    if skip_duplicates:
        statement += " ON CONFLICT DO NOTHING"

    # Using multi values requires us to parse the CSV in Python, as we will
    # need to bind individual columns.
    csv_reader = csv.reader(file)

    psycopg2.extras.execute_values(cursor, statement, csv_reader, page_size=page_size)


def load_csv(cursor, file, table):
    # To avoid any SQL injection, we ensure we quote the table name correctly.
    statement = "COPY {0} FROM STDIN WITH CSV".format(quote_ident(table, cursor))
    cursor.copy_expert(statement, file)


def generate_csv(cursor, file, records=100000):
    # With copy we cannot use placeholders (%s) to inject variables, therefore we're using mogrify to
    # correctly build up the SQL text to avoid any chance of SQL injection.
    # http://initd.org/psycopg/docs/cursor.html#cursor.mogrify
    #
    # We generate a dataset with a reasonable chance of collisions in the timestamp/key, to ensure
    # we will have unique key violations
    statement = cursor.mogrify("""
            SELECT
                date_trunc('minute', (now() - random() * interval '4 weeks')) AS measured,
                substr(md5(random():: text), 1, 2) AS key,
                random() AS value
            FROM
                generate_series(1, %s)
            """, (records,))

    cursor.copy_expert("COPY ({}) TO STDOUT WITH CSV".format(statement), file)


def main(args):
    conn_args = dict((k, args.get(k))
                     for k in ['user', 'dbname', 'port', 'host', 'service'])
    conn, cursor = connect(conn_args)

    if args['drop']:
        logging.info("Dropping table demo".format(**args))
        cursor.execute("DROP TABLE demo")

    if args['generate']:
        started = time.time()
        logging.info("Generating CSV with {generate} records of data".format(**args))
        generate_csv(cursor, sys.stdout, args['generate'])
        logging.info("Generated successfully, duration: {0}".format(time.time() - started))
        return

    if args['create']:
        prepare_table(cursor, args['table'], unique=(args['create'] == 'unique'))

    if args['create'] or args['drop']:
        cursor.execute('COMMIT')
        return

    started = time.time()
    if args['multi']:
        load_multi(cursor, args['file'], args['table'], page_size=args['multi'], skip_duplicates=args['skip_duplicates'])
        logging.info(
            "Loaded data using multi-values insert statement, page_size={0}, skip_duplicates={1}, duration: {2}".format(args['multi'], args['skip_duplicates'], time.time() - started))
    else:
        load_csv(cursor, args['file'], args['table'])
        logging.info("Loaded data using COPY statement, rows: {0} duration: {1}".format(cursor.rowcount, time.time() - started))

    cursor.execute('COMMIT')
    conn.close()


if __name__ == '__main__':  # pragma: no cover
    args = vars(parse_arguments(sys.argv[1:]))

    loglevels = {'DEBUG': 10, 'INFO': 20, 'WARNING': 30, 'ERROR': 40, 'CRITICAL': 50}
    loglevel = loglevels['WARNING']
    if args['loglevel']:
        loglevel = loglevels.get(args['loglevel'].upper()) or int(args['loglevel'])
    loglevel -= args['verbose'] * 10
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=loglevel)
    main(args)
